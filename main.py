# Libraries
from tkinter import *
from PIL import Image, ImageTk
import imutils
import cv2
import numpy as np
from ultralytics import YOLO
import math

def clean_lbl():
    # Clean
    lblimg.config(image='')
    lblimgtxt.config(image='')

def images(img, imgtxt):
    img = img
    imgtxt = imgtxt

    # Img Detect
    img = np.array(img, dtype="uint8")
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    img = Image.fromarray(img)

    img_ = ImageTk.PhotoImage(image=img)
    lblimg.configure(image=img_)
    lblimg.image = img_

    # Img Text
    imgtxt = np.array(imgtxt, dtype="uint8")
    imgtxt = cv2.cvtColor(imgtxt, cv2.COLOR_BGR2RGB)
    imgtxt = Image.fromarray(imgtxt)

    img_txt = ImageTk.PhotoImage(image=imgtxt)
    lblimgtxt.configure(image=img_txt)
    lblimgtxt.image = img_txt

# Scanning Function
def Scanning():
    global img_metal, img_vidrio, img_plastico, img_carton, img_riesgobio
    global img_metaltxt, img_vidriotxt, img_plasticotxt, img_cartontxt, img_riesgobiotxt, pantalla
    global lblimg, lblimgtxt

    # Interfaz
    lblimg = Label(pantalla)
    lblimg.place(x=75, y=260)

    lblimgtxt = Label(pantalla)
    lblimgtxt.place(x=995, y=310)
    detect = False

    # Read VideoCapture
    if cap is not None:
        ret, frame = cap.read()
        frame_show =cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        # True
        if ret == True:
            # Yolo | AntiSpoof
            results = model(frame, stream=True, verbose=False)
            for res in results:
                # Box
                boxes = res.boxes
                for box in boxes:
                    detect = True
                    # Bounding box
                    x1, y1, x2, y2 = box.xyxy[0]
                    x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)

                    # Error < 0
                    if x1 < 0: x1 = 0
                    if y1 < 0: y1 = 0
                    if x2 < 0: x2 = 0
                    if y2 < 0: y2 = 0

                    # Class
                    cls = int(box.cls[0])

                    # Confidence
                    conf = math.ceil(box.conf[0])
                    #print(f"Clase: {cls} Confidence: {conf}")
                    #Metal
                    if cls == 0:
                        #draw rectangulo
                        cv2.rectangle(frame_show, (x1, y1), (x2, y2), (155, 155, 155), 2)

                        #text
                        text = f'{clsName[cls]} {int(conf) * 100}%'
                        sizetext = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
                        dim = sizetext[0]
                        baseline = sizetext[1]

                        #rect
                        cv2.rectangle(frame_show, (x1, y1 - dim[1] - baseline), (x1 + dim[0], y1 + baseline), (0,0,0), cv2.FILLED)
                        cv2.putText(frame_show, text, (x1, y1 - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (155, 155, 155), 2)

                        #imagen
                        images(img_metal, img_metaltxt)

                        #Vidrio
                    if cls == 1:
                        #draw rectangulo
                        cv2.rectangle(frame_show, (x1, y1), (x2, y2), (255, 255, 255), 2)

                        #text
                        text = f'{clsName[cls]} {int(conf) * 100}%'
                        sizetext = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
                        dim = sizetext[0]
                        baseline = sizetext[1]

                        #rect
                        cv2.rectangle(frame_show, (x1, y1 - dim[1] - baseline), (x1 + dim[0], y1 + baseline), (0,0,0), cv2.FILLED)
                        cv2.putText(frame_show, text, (x1, y1 - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

                        # imagen
                        images(img_vidrio, img_vidriotxt)

                    #Plastico
                    if cls == 2:
                        #draw rectangulo
                        cv2.rectangle(frame_show, (x1, y1), (x2, y2), (0, 255, 0), 2)

                        #text
                        text = f'{clsName[cls]} {int(conf) * 100}%'
                        sizetext = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
                        dim = sizetext[0]
                        baseline = sizetext[1]

                        #rect
                        cv2.rectangle(frame_show, (x1, y1 - dim[1] - baseline), (x1 + dim[0], y1 + baseline), (0,0,0), cv2.FILLED)
                        cv2.putText(frame_show, text, (x1, y1 - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

                        # imagen
                        images(img_plastico, img_plasticotxt)

                    #RiesgoBio
                    if cls == 3:
                        #draw rectangulo
                        cv2.rectangle(frame_show, (x1, y1), (x2, y2), (255, 0, 0), 2)

                        #text
                        text = f'{clsName[cls]} {int(conf) * 100}%'
                        sizetext = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
                        dim = sizetext[0]
                        baseline = sizetext[1]

                        #rect
                        cv2.rectangle(frame_show, (x1, y1 - dim[1] - baseline), (x1 + dim[0], y1 + baseline), (0,0,0), cv2.FILLED)
                        cv2.putText(frame_show, text, (x1, y1 - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)

                        # imagen
                        images(img_carton, img_cartontxt)

                    #Carton
                    if cls == 4:
                        #draw rectangulo
                        cv2.rectangle(frame_show, (x1, y1), (x2, y2), (255, 255, 0), 2)

                        #text
                        text = f'{clsName[cls]} {int(conf) * 100}%'
                        sizetext = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
                        dim = sizetext[0]
                        baseline = sizetext[1]

                        #rect
                        cv2.rectangle(frame_show, (x1, y1 - dim[1] - baseline), (x1 + dim[0], y1 + baseline), (0,0,0), cv2.FILLED)
                        cv2.putText(frame_show, text, (x1, y1 - 5), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2)

                        # imagen
                        images(img_riesgobio, img_riesgobiotxt)

                    if detect == False:
                        # Clean
                        clean_lbl()


            # Resize
            frame_show = imutils.resize(frame_show, width=640)

            # Convertimos el video
            im = Image.fromarray(frame_show)
            img = ImageTk.PhotoImage(image=im)

            # Mostramos en el GUI
            lblVideo.configure(image=img)
            lblVideo.image = img
            lblVideo.after(10, Scanning)

        else:
            cap.release()

# main
def ventana_principal():
    global model, clsName, img_metal, img_vidrio, img_carton, img_riesgobio, img_plastico, lblVideo
    global img_metaltxt, img_cartontxt, img_vidriotxt, img_riesgobiotxt, img_plasticotxt, cap, pantalla
    # Ventana principal
    pantalla = Tk()
    pantalla.title("RECICLAJE INTELIGENTE")
    pantalla.geometry("1280x720")

    # Background
    imagenF = PhotoImage(file="setUp/template.png")
    background = Label(image=imagenF, text="Inicio")
    background.place(x=0, y=0, relwidth=1, relheight=1)

    # Clases: 0 -> Metal | 1 -> Glass | 2 -> Plastic | 3 -> Carton | 4 -> Medical
    # Model
    model = YOLO('Modelos/best.pt')

    # Clases
    clsName = ['Metal', 'Vidrio', 'Plastico', 'Carton', 'Riesgo Biologico']

    # Images
    img_metal = cv2.imread("SetUp/Metal.png")
    img_carton = cv2.imread("SetUp/Carton.png")
    img_vidrio = cv2.imread("SetUp/Vidrio.png")
    img_riesgobio = cv2.imread("SetUp/RiesgoBio.png")
    img_plastico = cv2.imread("SetUp/Plastico.png")
    img_metaltxt = cv2.imread("SetUp/metaltxt.png")
    img_cartontxt = cv2.imread("SetUp/cartontxt.png")
    img_vidriotxt = cv2.imread("SetUp/vidriotxt.png")
    img_riesgobiotxt = cv2.imread("SetUp/riesgobiotxt.png")
    img_plasticotxt = cv2.imread("SetUp/plasticotxt.png")

    # Video
    lblVideo = Label(pantalla)
    lblVideo.place(x=320, y=165)

    # Elegimos la camara
    cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
    cap.set(3, 600)
    cap.set(4, 400)
    Scanning()

    # Eject
    pantalla.mainloop()

if __name__ == "__main__":
    ventana_principal()